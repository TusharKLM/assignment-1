var numbers = [1, 4, 9, 0, 15];
var individualAddition = numbers.map(function(num) {
  if(num!=0) return num += 5;
  else return num;
});
console.log(individualAddition);

Description: The Map function calls a provided callback function once for each element in an array, in order, and constructs a new array from the results.
			 Here, we are calling a callback function which checks if an element is not 0, we are adding 5 to it. Hence, it will create a new array containing the new digits created from previous array without changing the 0 value.