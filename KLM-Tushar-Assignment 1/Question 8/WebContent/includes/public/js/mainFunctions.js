$(document).ready(function(){
	$('.t-header--btn').click(function(){
		$('header').toggleClass('is-open');
		$('footer').toggleClass('is-open');
		$('.t-header--btn').toggleClass('is-open-img');
		if ($('.is-open-img').length) {
			$('.t-header--btn img').attr('src','../../../includes/public/images/Close.svg');
		}
		else {
			$('.t-header--btn img').attr('src','../../../includes/public/images/Hamburger.svg');
			$('.o-left-panel .c-header__toggleButton').css('background', '#00a1de');
		}
	});
	$('.t-header--closeLink').click(function(){
		$('header').removeClass('is-open');
		$('footer').removeClass('is-open');
		$('.t-header--btn img').attr('src','../../../includes/public/images/Hamburger.svg');
	})
	
	$(".color-contrast").click(function(){	
		if($(this).hasClass("t-black--yellow-contrast"))	{
			$("link[title='high contrast']").attr("href","../../../includes/public/scss/highContrastYellow.css");
		}
		else if($(this).hasClass("t-yellow--black-contrast"))	{
			$("link[title='high contrast']").attr("href","../../../includes/public/scss/highContrastBlack.css");
		}
		else
		{
			$("link[title='high contrast']").attr("href","");
		}
	});	
});